epicsEnvSet(PLCNAME, "LNS-LEBT-010:VAC-PLC-11111")
epicsEnvSet(IPADDR, "10.10.1.61")
epicsEnvSet(S7DRVPORT, "2000")
epicsEnvSet(MODBUSDRVPORT, "502")
epicsEnvSet(INSIZE, "2000")
epicsEnvSet(RECVTIMEOUT, "1000")
require plc_lns_lebt_010_vac_plc_11111, 2.0.1-catania
< ${REQUIRE_plc_lns_lebt_010_vac_plc_11111_PATH}/startup/startup.cmd





epicsEnvSet(DEVICENAME, "LNS-LEBT-010:VAC-VEG-10010")
epicsEnvSet(IPADDR, "10.10.1.21")
epicsEnvSet(PORT, "4001")
require vac_ctrl_mks946_937b, 2.0.0-catania
< ${REQUIRE_vac_ctrl_mks946_937b_PATH}/startup/vac_ctrl_mks946_937b_ethernet.cmd

epicsEnvSet(DEVICENAME, "LNS-LEBT-010:VAC-VGP-10000")
epicsEnvSet(CONTROLLERNAME, "LNS-LEBT-010:VAC-VEG-10010")
epicsEnvSet(CHANNEL, "1")
epicsEnvSet(CHANNEL_C, "1")
epicsEnvSet(RELAY1, "1")
epicsEnvSet(RELAY2, "2")
require vac_gauge_mks_vgp, 2.0.0-catania
< ${REQUIRE_vac_gauge_mks_vgp_PATH}/startup/vac_gauge_mks_vgp.cmd

epicsEnvSet(DEVICENAME, "LNS-LEBT-010:VAC-VGC-10000")
epicsEnvSet(CONTROLLERNAME, "LNS-LEBT-010:VAC-VEG-10010")
epicsEnvSet(CHANNEL, "3")
epicsEnvSet(CHANNEL_C, "3")
epicsEnvSet(RELAY1, "5")
epicsEnvSet(RELAY2, "6")
epicsEnvSet(RELAY3, "7")
epicsEnvSet(RELAY4, "8")
require vac_gauge_mks_vgc, 2.0.0-catania
< ${REQUIRE_vac_gauge_mks_vgc_PATH}/startup/vac_gauge_mks_vgc.cmd

epicsEnvSet(DEVICENAME, "LNS-LEBT-010:VAC-VGP-00021")
epicsEnvSet(CONTROLLERNAME, "LNS-LEBT-010:VAC-VEG-10010")
epicsEnvSet(CHANNEL, "5")
epicsEnvSet(CHANNEL_C, "5")
epicsEnvSet(RELAY1, "9")
epicsEnvSet(RELAY2, "10")
require vac_gauge_mks_vgp, 2.0.0-catania
< ${REQUIRE_vac_gauge_mks_vgp_PATH}/startup/vac_gauge_mks_vgp.cmd

epicsEnvSet(DEVICENAME, "LNS-LEBT-010:VAC-VGP-00031")
epicsEnvSet(CONTROLLERNAME, "LNS-LEBT-010:VAC-VEG-10010")
epicsEnvSet(CHANNEL, "6")
epicsEnvSet(CHANNEL_C, "6")
epicsEnvSet(RELAY1, "11")
epicsEnvSet(RELAY2, "12")
require vac_gauge_mks_vgp, 2.0.0-catania
< ${REQUIRE_vac_gauge_mks_vgp_PATH}/startup/vac_gauge_mks_vgp.cmd





epicsEnvSet(DEVICENAME, "LNS-LEBT-010:VAC-VEVMC-01100")
epicsEnvSet(IPADDR, "10.10.1.21")
epicsEnvSet(PORT, "4004")
require vac_ctrl_mks946_937b, 2.0.0-catania
< ${REQUIRE_vac_ctrl_mks946_937b_PATH}/startup/vac_ctrl_mks946_937b_ethernet.cmd

epicsEnvSet(DEVICENAME, "LNS-LEBT-010:VAC-VVMC-01100")
epicsEnvSet(CONTROLLERNAME, "LNS-LEBT-010:VAC-VEVMC-01100")
epicsEnvSet(CHANNEL, "1")
require vac_mfc_mks_gv50a, 2.0.4-catania
< ${REQUIRE_vac_mfc_mks_gv50a_PATH}/startup/vac_mfc_mks_gv50a.cmd

epicsEnvSet(DEVICENAME, "LNS-LEBT-010:VAC-VVMC-04100")
epicsEnvSet(CONTROLLERNAME, "LNS-LEBT-010:VAC-VEVMC-01100")
epicsEnvSet(CHANNEL, "2")
require vac_mfc_mks_gv50a, 2.0.4-catania
< ${REQUIRE_vac_mfc_mks_gv50a_PATH}/startup/vac_mfc_mks_gv50a.cmd

epicsEnvSet(DEVICENAME, "LNS-LEBT-010:VAC-VGD-10000")
epicsEnvSet(CONTROLLERNAME, "LNS-LEBT-010:VAC-VEVMC-01100")
epicsEnvSet(CHANNEL, "3")
epicsEnvSet(CHANNEL_C, "3")
epicsEnvSet(RELAY1, "5")
epicsEnvSet(RELAY2, "6")
require vac_gauge_mks_vgd, 2.0.1-catania
< ${REQUIRE_vac_gauge_mks_vgd_PATH}/startup/vac_gauge_mks_vgd.cmd





epicsEnvSet(DEVICENAME, "LNS-LEBT-010:VAC-VEPT-02100")
epicsEnvSet(IPADDR, "10.10.1.21")
epicsEnvSet(PORT, "4002")
require vac_ctrl_leyboldtd20, 2.0.0-catania
< ${REQUIRE_vac_ctrl_leyboldtd20_PATH}/startup/vac_ctrl_leyboldtd20_ethernet.cmd





epicsEnvSet(DEVICENAME, "LNS-LEBT-010:VAC-VEPT-03100")
epicsEnvSet(IPADDR, "10.10.1.21")
epicsEnvSet(PORT, "4003")
require vac_ctrl_leyboldtd20, 2.0.0-catania
< ${REQUIRE_vac_ctrl_leyboldtd20_PATH}/startup/vac_ctrl_leyboldtd20_ethernet.cmd





epicsEnvSet(DEVICENAME, "LNS-LEBT-010:VAC-ECATIO-001")
require ecatio_lns_lebt_010_vac_ecatio_001, 2.0.0-catania
< ${REQUIRE_ecatio_lns_lebt_010_vac_ecatio_001_PATH}/startup/startup.cmd

